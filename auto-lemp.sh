#!/usr/bin/env bash

#UBUNTU VERSION ARRAYS
VERSIONARRAY=(16.04 16.04.1 16.04.2 16.04.3 16.04.4 16.04.5 16.04.6 18.04 18.04.1 18.04.2)
SIXTEENARRAY=(16.04 16.04.1 16.04.2 16.04.3 16.04.4 16.04.5 16.04.6)
EIGHTEENARRAY=(18.04 18.04.1 18.04.2)

#CHECKING UBUNTU VERSION
echo -e "######## \e[32mCHECKING UBUNTU VERSION\e[39m ########"
VERSION=$(lsb_release -rs)

if [[ ! " ${VERSIONARRAY[@]} " =~ " ${VERSION} " ]]; then
    echo -e ""
    echo -e "This script does nopt support your OS/Distribution."
    echo -e "Goodbye!"
    echo -e ""
    exit 1
fi

echo -e ""
echo -e "Found Ubuntu Version: $VERSION"

#INSTALLING UPDATES
sudo apt-get update -y -qq > /dev/null
echo -e ""

#INSTALLING NGINX
echo -e "######## \e[32mINSTALLING NGINX\e[39m ########"
sudo apt install -y nginx > /dev/null

#CONFIGURING UFW
echo -e "######## \e[32mCONFIGURING UFW\e[39m ########"
echo -e ""
sudo ufw allow in "Nginx HTTP"
echo -e ""

#INSTALLING MYSQL
if [[ " ${SIXTEENARRAY[@]} " =~ " ${VERSION} " ]]; then
    echo -e "######## \e[32mINSTALLING MYSQL FOR UBUNTU 16\e[39m ########"
    echo -e ""
    read -s -p "What Password Do You Want To Set For Mysql?: " mysqlpass
    echo -e ""
    echo "mysql-server mysql-server/root_password password $mysqlpass" | sudo debconf-set-selections
    echo "mysql-server mysql-server/root_password_again password $mysqlpass" | sudo debconf-set-selections
    sudo apt-get install -y mysql-server -qq > /dev/null
    echo -e ""
fi

if [[ " ${EIGHTEENARRAY[@]} " =~ " ${VERSION} " ]]; then
    echo -e "######## \e[32mINSTALLING MYSQL FOR UBUNTU 18\e[39m ########"
    echo -e ""
    read -s -p "What Password Do You Want To Set For Mysql?: " mysqlpass
    echo -e ""
    sudo apt install -y mysql-server -qq > /dev/null
    mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$mysqlpass';"
fi

#CONFIGURING MYSQL
echo -e "######## \e[32mCONFIGURING MYSQL\e[39m ########"
echo -e ""
mysql -u root -p$mysqlpass -e "DELETE FROM mysql.user WHERE User='';"
mysql -u root -p$mysqlpass -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
mysql -u root -p$mysqlpass -e "DROP DATABASE IF EXISTS test;"
mysql -u root -p$mysqlpass -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';"
mysql -u root -p$mysqlpass -e "FLUSH PRIVILEGES;"
echo -e ""

#INSTALLING PHP
echo -e "######## \e[32mINSTALLING PHP\e[39m ########"
sudo apt install -y php-fpm php-mysql -qq > /dev/null
echo -e ""

#CONFIGURING NGINX FOR PHP
echo -e "######## \e[32mCONFIGURING NGINX FOR PHP\e[39m ########"
echo -e ""
read -p "Enter your domain name or leave blank for none: " domain
echo -e ""
echo -e ""

if [ -z "$domain" ];
then
domain="example.com"
fi

if [[ " ${SIXTEENARRAY[@]} " =~ " ${VERSION} " ]]; then
    echo "server {" > /etc/nginx/sites-available/$domain
    echo "    listen 80 default_server;" >> /etc/nginx/sites-available/$domain
    echo "    listen [::]:80 default_server;" >> /etc/nginx/sites-available/$domain
    echo " " >> /etc/nginx/sites-available/$domain
    echo "    root /var/www/html;" >> /etc/nginx/sites-available/$domain
    echo "    index index.php index.html index.htm index.nginx-debian.html;" >> /etc/nginx/sites-available/$domain
    echo " " >> /etc/nginx/sites-available/$domain
    echo "    server_name $domain;" >> /etc/nginx/sites-available/$domain
    echo " " >> /etc/nginx/sites-available/$domain
    echo "    location / {" >> /etc/nginx/sites-available/$domain
    echo "        try_files \$uri \$uri/ =404;" >> /etc/nginx/sites-available/$domain
    echo "    }" >> /etc/nginx/sites-available/$domain
    echo " " >> /etc/nginx/sites-available/$domain
    echo "    location ~ \.php$ {" >> /etc/nginx/sites-available/$domain
    echo "        include snippets/fastcgi-php.conf;" >> /etc/nginx/sites-available/$domain
    echo "        fastcgi_pass unix:/run/php/php7.0-fpm.sock;" >> /etc/nginx/sites-available/$domain
    echo "    }" >> /etc/nginx/sites-available/$domain
    echo " " >> /etc/nginx/sites-available/$domain
    echo "    location ~ /\.ht {" >> /etc/nginx/sites-available/$domain
    echo "        deny all;" >> /etc/nginx/sites-available/$domain
    echo "    }" >> /etc/nginx/sites-available/$domain
    echo "}" >> /etc/nginx/sites-available/$domain
    sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php/7.0/fpm/php.ini
    sudo systemctl restart php7.0-fpm
fi

if [[ " ${EIGHTEENARRAY[@]} " =~ " ${VERSION} " ]]; then
    echo "server {" > /etc/nginx/sites-available/$domain
    echo "        listen 80;" >> /etc/nginx/sites-available/$domain
    echo "        root /var/www/html;" >> /etc/nginx/sites-available/$domain
    echo "        index index.php index.html index.htm index.nginx-debian.html;" >> /etc/nginx/sites-available/$domain
    echo "        server_name $domain;" >> /etc/nginx/sites-available/$domain
    echo " " >> /etc/nginx/sites-available/$domain
    echo "        location / {" >> /etc/nginx/sites-available/$domain
    echo "                try_files \$uri \$uri/ =404;" >> /etc/nginx/sites-available/$domain
    echo "        }" >> /etc/nginx/sites-available/$domain
    echo " " >> /etc/nginx/sites-available/$domain
    echo "        location ~ \.php$ {" >> /etc/nginx/sites-available/$domain
    echo "                include snippets/fastcgi-php.conf;" >> /etc/nginx/sites-available/$domain
    echo "                fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;" >> /etc/nginx/sites-available/$domain
    echo "        }" >> /etc/nginx/sites-available/$domain
    echo " " >> /etc/nginx/sites-available/$domain
    echo "        location ~ /\.ht {" >> /etc/nginx/sites-available/$domain
    echo "                deny all;" >> /etc/nginx/sites-available/$domain
    echo "        }" >> /etc/nginx/sites-available/$domain
    echo "}" >> /etc/nginx/sites-available/$domain
fi

sudo ln -s /etc/nginx/sites-available/$domain /etc/nginx/sites-enabled/
sudo unlink /etc/nginx/sites-enabled/default
sudo systemctl reload nginx

#INSTALLATION FINISHED
echo -e "######## \e[32mINSTALLATION FINISHED\e[39m ########"
echo -e ""
echo -e "Congratulations, installion is complete!"
echo -e ""

