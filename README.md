## A Continuation. 
After discussing it with a friend of mine I come to realise that you can't really post a script on how to set up **LAMP** servers without releasing a script to cover **LEMP** servers too.
## The Script.
The script itself will install NginX, install MySQL and run the MySQL Secure Installation, Install and configure PHP. This script is compatible with any Ubuntu 16 & 18 release and will adjust the installation requirements depending on which OS is detected. There's not really a lot else to say except Enjoy!
## Get the Script.
```
cd ~
git clone https://gitlab.com/Sutherland/lemp-auto-install-ubuntu.git
```
## Run the Script.
```
cd ~/lemp-auto-install-ubuntu
sudo bash ./auto-lemp.sh
```
During the installation you will be asked to specify the password you would like set for the root MySQL user. You will also be asked if you want to set your host name for Nginx Configuration. 
If you read this and you would like to give any feedback or discuss the script any further, please email sutherland@scripting.online.